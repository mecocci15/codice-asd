#include "sorting.h"
#include "array.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void test_sorting_debug(int arr[], int n, void (*alg)(int arr[], int n)) {
  printf("Prima: ");
  print_array(arr, n);

  (*alg)(arr, n);

  printf("Dopo: ");
  print_array(arr, n);
  printf("\n");
}

int check_sorted(int *arr, int n) {
  int sorted = 1;

  for (int i = 0; sorted && i < n - 1; i++) {
    sorted = sorted && (arr[i] <= arr[i + 1]);
  }
  
  return sorted;
}

void test_sorting_arr(int arr[], int n, void (*alg)(int arr[], int n)) {
  (*alg)(arr, n);
  
  if (!check_sorted(arr, n)) {
    printf("ERR\n");
  } else {
    printf("OK\n");
  }
}

void test_sorting(void (*alg)(int arr[], int n)) {
  int even_arr[4] = {3, 7, 2, 5};
  int odd_arr[5] = {4, 2, 8, 9, 1};
  int dup_arr[5] = {4, 2, 8, 4, 1};

  printf("-- Array lunghezza pari: ");
  test_sorting_arr(even_arr, 4, alg);

  printf("-- Array lunghezza dispari: ");
  test_sorting_arr(odd_arr, 5, alg);

  printf("-- Array con duplicato: ");
  test_sorting_arr(dup_arr, 5, alg);
}

int main(int argc, char const *argv[]) {
  test_sorting(quick_sort);

  return 0;
}
