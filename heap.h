#ifndef HEAP_H
#define HEAP_H

typedef struct heap {
  int heap_size;
  int length;
  int *arr;
} heapFrame;

typedef heapFrame *Heap;

Heap heap_create(int *arr2, int capacity);

void heap_increase_key(Heap h, int i, int key);

void heap_insert(Heap h, int key);

void heap_print(Heap h);

void heap_build(Heap h);

void heap_sort(Heap h);

int heap_get_max(Heap h);

int heap_extract_max(Heap h);

#endif