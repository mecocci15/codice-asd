#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node {
  int key;
  struct Node *child;
  struct Node *sibling;
} Node;

typedef Node *KTree;

KTree ktree_cons(int key, KTree child, KTree sibling) {
  KTree new = malloc(sizeof(Node));

  new->key = key;
  new->child = child;
  new->sibling = sibling;

  return new;
}

void ktree_print(KTree t, int n) {
  if (t != NULL) {
    for (int i = 0; i < n; i++) {
      printf("  ");
    }

    printf("%d\n", t->key);

    KTree cl = t->child;

    while (cl != NULL) {
      ktree_print(cl, n + 1);
      cl = cl->sibling;
    }
  }
}

int sumLeaves(KTree t) {
  if (t == NULL) {
    return 0;
  } else {
    if (t->child == NULL) {
      return t->key + sumLeaves(t->sibling);
    } else {
      return sumLeaves(t->child) + sumLeaves(t->sibling);
    }
  }
}

int max(int a, int b) {
  if (a > b) {
    return a;
  } else {
    return b;
  }
}

// pre: t non vuoto
// post: altezza di t (max dei suoi livelli)
int ktree_height(KTree t) {
  if (t->child == NULL && t->sibling == NULL) {
    return 0;
  } else if (t->child != NULL && t->sibling == NULL) {
    return 1 + ktree_height(t->child);
  } else if (t->child == NULL && t->sibling != NULL) {
    return ktree_height(t->sibling);
  } else {
    int child_height = 1 + ktree_height(t->child);
    int sibling_height = ktree_height(t->sibling);

    return max(child_height, sibling_height);
  }
}

int ktree_card(KTree t) {
  if (t == NULL) {
    return 0;
  } else {
    return 1 + ktree_card(t->child) + ktree_card(t->sibling);
  }
}

// ---------------------------------------
//  Visita DFS con pila
// ---------------------------------------

typedef KTree Type;

typedef struct stackEl {
  Type info;
  struct stackEl *next;
} StackEl;

struct stackFrame {
  StackEl *top;
};

typedef struct stackFrame *Stack;

Stack stack_new() {
  Stack s = malloc(sizeof(struct stackFrame));

  s->top = NULL;

  return s;
}

void stack_push(Stack s, Type x) {
  StackEl *new_top = malloc(sizeof(StackEl));

  new_top->info = x;
  new_top->next = s->top;

  s->top = new_top;
}

Type stack_top(Stack s) { return s->top->info; }

Type stack_pop(Stack s) {
  Type old_top = s->top->info;

  s->top = s->top->next;

  return old_top;
}

void ktree_dfs(KTree t) {
  Stack s = stack_new();
  Type t2;
  stack_push(s, t);

  printf("Visita DFS: ");

  while (s->top != NULL) {
    // visito il nodo
    t2 = stack_pop(s);
    printf("%d ", t2->key);

    // pusho tutti i figli di t2 sullo stack
    if (t2->child != NULL) {
      stack_push(s, t2->child);

      Type c = t2->child->sibling;
      while (c != NULL) {
        stack_push(s, c);
        c = c->sibling;
      }
    }
  }

  printf("\n");
}

// ---------------------------------------
//  Visita DFS con coda
// ---------------------------------------

typedef KTree Type;

typedef struct queueEl {
  Type info;
  struct queueEl *next;
} QueueEl;

struct queueFrame {
  QueueEl *front;
  QueueEl *rear;
};

typedef struct queueFrame *Queue;

// post: alloca e ritorna una coda vuota
Queue queue_new() {
  Queue q = malloc(sizeof(struct queueFrame));

  q->front = NULL;
  q->rear = NULL;

  return q;
}

int queue_isempty(Queue q) { return q->front == NULL; }

Type queue_first(Queue q) { return q->front->info; }

void queue_enqueue(Queue q, Type x) {
  QueueEl *elem = malloc(sizeof(QueueEl));

  elem->info = x;
  elem->next = NULL;

  if (queue_isempty(q)) {
    q->front = q->rear = elem;
  } else {
    q->rear->next = elem;
    q->rear = elem;
  }
}

Type queue_dequeue(Queue q) {
  QueueEl *oldFront = q->front;
  Type removed = q->front->info;

  if (q->front == q->rear) {
    q->front = q->rear = NULL;
  } else {
    q->front = q->front->next;
  }

  free(oldFront);

  return removed;
}

void ktree_bfs(KTree t) {
  Queue q = queue_new();
  Type t2;
  queue_enqueue(q, t);

  printf("Visita BFS: ");

  while (!queue_isempty(q)) {
    // visito il nodo
    t2 = queue_dequeue(q);
    printf("%d ", t2->key);

    // pusho tutti i figli di t2 sullo stack
    if (t2->child != NULL) {
      queue_enqueue(q, t2->child);

      Type c = t2->child->sibling;
      while (c != NULL) {
        queue_enqueue(q, c);
        c = c->sibling;
      }
    }
  }

  printf("\n");
}

int main() {
  KTree a = ktree_cons(3, NULL, NULL);
  KTree b = ktree_cons(6, NULL, a);
  KTree d = ktree_cons(8, ktree_cons(4, NULL, NULL), NULL);
  KTree c = ktree_cons(5, b, d);
  KTree f = ktree_cons(1, NULL, c);

  KTree t = ktree_cons(2, f, NULL);

  ktree_print(t, 0);

  printf("\nSomma foglie: %d\n", sumLeaves(t));

  printf("\nAltezza: %d\n", ktree_height(t));

  printf("\nCardinalità: %d\n", ktree_card(t));

  printf("\n");
  ktree_dfs(t);

  printf("\n");
  ktree_bfs(t);

  return 0;
}