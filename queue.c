#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int Type;

typedef struct queueEl {
  Type info;
  struct queueEl *next;
} QueueEl;

struct queueFrame {
  QueueEl *front;
  QueueEl *rear;
};

typedef struct queueFrame *Queue;

// post: alloca e ritorna una coda vuota
Queue queue_new() {
  Queue q = malloc(sizeof(struct queueFrame));

  q->front = NULL;
  q->rear = NULL;

  return q;
}

int queue_isempty(Queue q) { return q->front == NULL; }

Type queue_first(Queue q) { return q->front->info; }

void queue_enqueue(Queue q, Type x) {
  QueueEl *elem = malloc(sizeof(QueueEl));

  elem->info = x;
  elem->next = NULL;

  if (queue_isempty(q)) {
    q->front = q->rear = elem;
  } else {
    q->rear->next = elem;
    q->rear = elem;
  }
}

Type queue_dequeue(Queue q) {
  QueueEl *oldFront = q->front;
  Type removed = q->front->info;

  if (q->front == q->rear) {
    q->front = q->rear = NULL;
  } else {
    q->front = q->front->next;
  }

  free(oldFront);

  return removed;
}

int main() {
  Queue q = queue_new();

  queue_enqueue(q, 1);
  queue_enqueue(q, 2);
  queue_enqueue(q, 3);

  for (int i = 0; i < 3; i++) {
    printf("%d\n", queue_dequeue(q));
  }

  return 0;
}