#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int linear_search(int x, int arr[], int n) {
  int i = 0;
  int found = -1;
  
  while (i < n && found == -1) {
    if (arr[i] == x) {
      found = i;
    }

    i++;
  }

  return found;
}

int binary_search_aux(int x, int arr[], int i, int j) {
  if (i > j) {
    return -1;
  } else {
    int m = (i + j) / 2;

    if (x == arr[m]) {
      return m;
    } else if (x > arr[m]) {
      return binary_search_aux(x, arr, m + 1, j);
    } else {
      return binary_search_aux(x, arr, i, m - 1);
    }
  }
}

int binary_search(int x, int arr[], int n) {
  return binary_search_aux(x, arr, 0, n - 1);
}

int main(int argc, char const *argv[]) {
  int array[5] = {1, 2, 3, 5, 8};
  int result = binary_search(4, array, 5);

  if (result != -1) {
    printf("Trovato: %d\n", result);
  } else {
    printf("Non trovato\n");
  }

  return 0;
}
