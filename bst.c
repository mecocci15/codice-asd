#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node {
  int key;
  struct Node *parent;
  struct Node *left;
  struct Node *right;
} Node;

typedef struct Node *BTree;

BTree btree_cons(int key, BTree p, BTree l, BTree r) {
  BTree rootnode = malloc(sizeof(Node));

  rootnode->parent = p;
  rootnode->key = key;
  rootnode->left = l;
  rootnode->right = r;

  return rootnode;
}

void btree_print_aux(BTree bt, int n) {
  if (bt != NULL) {
    for (int i = 0; i < n; i++)
      printf("   ");

    printf("%d\n", bt->key);
    btree_print_aux(bt->left, n + 1);
    btree_print_aux(bt->right, n + 1);
  }
}

void btree_print(BTree bt) { btree_print_aux(bt, 0); }

// ----------------------------------------
// parte degli alberi binari di ricerca
// ----------------------------------------

BTree bst_insert_rec(BTree t, int x) {
  if (t == NULL) {
    return btree_cons(x, NULL, NULL, NULL);
  } else {
    if (x < t->key) {
      t->left = bst_insert_rec(t->left, x);
      t->left->parent = t;
    } else if (x > t->key) {
      t->right = bst_insert_rec(t->right, x);
      t->right->parent = t;
    }

    return t;
  }
}

BTree bst_insert_iter(BTree T, int x) {
  BTree N = btree_cons(x, NULL, NULL, NULL);
  BTree P = NULL;
  BTree S = T;

  // inv. se P != NULL allora P è il padre di S
  while (S != NULL) {
    P = S;

    // la chiave già c'è
    if (N->key == S->key) {
      return T;
    } else {
      if (N->key < S->key) {
        S = S->left;
      } else {
        S = S->right;
      }
    }
  }

  // attacco (se serve) il padre al figlio e il
  // figlio al padre
  N->parent = P;

  if (P == NULL) {
    T = N;
  } else {
    if (N->key < P->key) {
      P->left = N;
    } else {
      P->right = N;
    }
  }

  return T;
}

// pre: t albero binario di ricerca non vuoto
// post: nodo s di t con s->key minimo
BTree bst_min(BTree t) {
  while (t->left != NULL) {
    t = t->left;
  }

  return t;
}

// pre: x chiave, t binario di ricerca
// post: il nodo s in t con s->key = x se esiste
//       NULL altrimenti
BTree bst_search_rec(BTree t, int x) {
  if (t == NULL) {
    return NULL;
  } else {
    if (t->key == x) {
      return t;
    } else if (x < t->key) {
      return bst_search_rec(t->left, x);
    } else { // x > t->key
      return bst_search_rec(t->right, x);
    }
  }
}

// pre: x chiave, t binario di ricerca
// post: il nodo s in t con s->key = x se esiste
//       NULL altrimenti
BTree bst_search_iter(BTree t, int x) {
  while (t != NULL && t->key != x) {
    if (x < t->key) {
      t = t->left;
    } else {
      t = t->right;
    }
  }

  return t;
}

// pre: n nodo di un albero binario di ricerca
// post: il successore di n se esiste
//       altrimenti NULL
BTree bst_successor(BTree n) {
  if (n->right != NULL) {
    return bst_min(n->right);
  } else {
    BTree p = n->parent;

    while (p != NULL && n == p->right) {
      n = p;
      p = n->parent;
    }

    return p;
  }
}

// pre: t binario di ricerca, u e v nodi di t
// post: v trapiantato al posto di u
BTree transplant(BTree t, BTree u, BTree v) {
  if (u->parent == NULL) {
    t = v;
  } else if (u == u->parent->left) {
    u->parent->left = v;
  } else {
    u->parent->right = v;
  }

  if (v != NULL) {
    v->parent = u->parent;
  }

  return t;
}

// pre: t albero binario non vuoto, z nodo di t
// post: albero binario di ricerca senza z
BTree bst_delete(BTree t, BTree z) {
  if (z->left == NULL) {
    return transplant(t, z, z->right);
  } else if (z->right == NULL) {
    return transplant(t, z, z->left);
  } else {
    BTree y = bst_min(z->right);

    if (y->parent != z) {
      t = transplant(t, y, y->right);
      y->right = z->right;
      y->right->parent = y;
    }

    t = transplant(t, z, y);
    y->left = z->left;
    y->left->parent = y;

    return t;
  }
}

// -----------------------------------------
// BST to Lista ordinata dei vertici
// ----------------------------------------- 

typedef struct ListEl {
  int key;
  struct ListEl *next;
} ListEl;

typedef ListEl *List;

List cons(int x, List xs) {
  ListEl *new_node = malloc(sizeof(ListEl));

  new_node->key = x;
  new_node->next = xs;

  return new_node;
}

void list_print(List l) {
  while (l != NULL) {
    printf("%d ", l->key);
    l = l->next;
  }

  printf("\n");
}

// pre: t albero binario di ricerca
List bst_to_ordered_list(BTree t, List l) {
  if (t == NULL) {
    return l;
  } else {
    List r = bst_to_ordered_list(t->right, l);

    return bst_to_ordered_list(t->left, cons(t->key, r));
  }
}

int main() {
  int arr[] = {5, 15, 8, 20, 17, 16, 3, 4, 1, 2};
  BTree t = NULL;

  for (int i = 0; i < 10; i++) {
    t = bst_insert_iter(t, arr[i]);
  }

  printf("\nAlbero iniziale:\n");
  btree_print(t);

  printf("\nMinimo:\n");
  btree_print(bst_min(t));

  printf("\nSuccessore di 2:\n");
  btree_print(bst_successor(bst_search_iter(t, 2)));

  printf("\nCancellazione di 15:\n");
  BTree s = bst_search_iter(t, 15);
  btree_print(bst_delete(t, s));

  printf("\nLista ordinata dei vertici: ");
  list_print(bst_to_ordered_list(t, NULL));

  return 0;
}
