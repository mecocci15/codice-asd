#include <stdio.h>
#include <stdlib.h>

void print_array(int *array, int size) {
  printf("[");

  for (int i = 0; i < size - 1; i++) {
    printf("%d, ", array[i]);
  }

  if (size > 0) {
    printf("%d", array[size - 1]);
  }

  printf("]\n");
}

int rand_int(int min, int max) { 
  return (rand() % (max - min)) + min; 
}