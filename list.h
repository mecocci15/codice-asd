#ifndef LIST_H
#define LIST_H

#define TRUE 1
#define FALSE 0

typedef struct Node {
  int key;
  struct Node *next;
} Node;

typedef Node *List;

List cons(int x, List xs);

void list_print(List l);

List list_append(List l, List m);

int list_length(List l);

List list_delete(List l, int x);
List list_delete_all(List l, int x);

int list_search(List l, int x);

List list_reverse_rec(List l);
List list_reverse_iter(List l);

List list_clone_iter(List l);
List list_clone_rec(List l);

List list_insert_at_iter(int x, int i, List l);
List list_insert_at_rec(int x, int i, List l);

List array_to_list(int arr[], int n);

#endif