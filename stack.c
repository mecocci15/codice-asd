#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int Type;

typedef struct stackEl {
  Type info;
  struct stackEl *next;
} StackEl;

struct stackFrame {
  StackEl *top;
};

typedef struct stackFrame *Stack;

Stack stack_new() {
  Stack s = malloc(sizeof(struct stackFrame));

  s->top = NULL;

  return s;
}

void stack_push(Stack s, Type x) {
  StackEl *new_top = malloc(sizeof(StackEl));

  new_top->info = x;
  new_top->next = s->top;

  s->top = new_top;
}

Type stack_top(Stack s) { return s->top->info; }

Type stack_pop(Stack s) {
  Type old_top = s->top->info;

  s->top = s->top->next;

  return old_top;
}

int main(int argc, char const *argv[]) {
  Stack s = stack_new();

  stack_push(s, 1);
  stack_push(s, 2);
  stack_push(s, 3);

  for (int i = 0; i < 3; i++) {
    printf("%d\n", stack_pop(s));
  }

  return 0;
}
