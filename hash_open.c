#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct hashFrame {
  int dim;    // dimensione della tabella
  int *array; // puntatore alla tabella array[0..dim-1]
};

typedef struct hashFrame *HashTable;

// pre:  m >= 1
// post: ritorna una tabella hash di dimensione m a chiavi positive,
//       inizializzata con tutti -1 che rappresenta l'assenza di chiavi
HashTable hashtable_new(int m) {
  HashTable T = malloc(sizeof(struct hashFrame));

  T->dim = m;
  T->array = malloc(m * sizeof(int));

  for (int i = 0; i < m; i++) {
    T->array[i] = -1;
  }

  return T;
}

// pre:  k >= 0 chiave, m >= 1 (dim. tabella)
// post: ritorna k mod m
int hash_func(int k, int m) { return k % m; }

// pre: chiave k >= 0
//      i >= 0 iterazione
//      m >= 1 (dim. tabella)
// post: ritorna (hash_func(k, m) + i) mod m
int linear_probing(int k, int i, int m) { return (hash_func(k, m) + i) % m; }

void hashtable_print(HashTable h) {
  for (int i = 0; i < h->dim; i++) {
    printf("%d : %d\n", i, h->array[i]);
  }
}

int hashtable_insert(HashTable h, int x) {
  int i = 0;

  while (i < h->dim) {
    int index = linear_probing(x, i, h->dim);

    if (h->array[index] == -1) {
      h->array[index] = x;
      return index;
    }

    i++;
  }

  // overflow
  return -2;
}

int hashtable_search(HashTable h, int x) {
  int i = 0;
  int index = linear_probing(x, i, h->dim);

  while (h->array[index] == -1 || i < h->dim) {
    if (h->array[index] == x) {
      return index;
    }

    i++;
    index = linear_probing(x, i, h->dim);
  }

  return -1;
}

void hashtable_delete(HashTable h, int x) {
  int index = hashtable_search(h, x);

  h->array[index] = -1;
}

int main() {
  HashTable h = hashtable_new(5);
  int r;

  for (int i = 5; i < 24; i += 5) {
    r = hashtable_insert(h, i);
  }

  printf("Ricerca di 5: %d\n", hashtable_search(h, 5));
  printf("Ricerca di 3: %d\n\n", hashtable_search(h, 3));

  hashtable_print(h);

  return 0;
}