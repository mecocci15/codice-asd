#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node {
  int val;
  struct node *next;
  struct node *prev;
} Node;

typedef Node *List;

struct hashFrame {
  int dim;     // dimensione della tabella
  List *array; // puntatore alla tabella array[0..dim-1]
};

typedef struct hashFrame *HashTable;

List cons(int x, List prev, List next) {
  List newlist = malloc(sizeof(Node));

  newlist->val = x;
  newlist->prev = prev;
  newlist->next = next;

  return newlist;
}

void list_print(List l) {
  while (l != NULL) {
    printf("%d -> ", l->val);
    l = l->next;
  }

  printf("NIL\n");
}

// pre:  m >= 1
// post: ritorna una tabella hash di dimensione m a chiavi positive,
//       inizializzata con tutti -1 che rappresenta l'assenza di chiavi
HashTable hashtable_new(int m) {
  HashTable T = malloc(sizeof(struct hashFrame));

  T->dim = m;
  T->array = malloc(m * sizeof(List));

  for (int i = 0; i < m; i++) {
    T->array[i] = NULL;
  }

  return T;
}

// pre:  k >= 0 chiave, m >= 1 (dim. tabella)
// post: ritorna k mod m
int hash_func(int k, int m) { return k % m; }

void hashtable_print(HashTable h) {
  for (int i = 0; i < h->dim; i++) {
    printf("%d : ", i);
    list_print(h->array[i]);
  }
}

void hashtable_insert(HashTable h, int x) {
  int index = hash_func(x, h->dim);

  h->array[index] = cons(x, NULL, h->array[index]);

  if (h->array[index]->next != NULL) {
    h->array[index]->next->prev = h->array[index];
  }
}

List hashtable_search(HashTable h, int x) {
  int index = hash_func(x, h->dim);
  List p = h->array[index];

  while (p != NULL) {
    if (p->val == x) {
      return p;
    }

    p = p->next;
  }

  return NULL;
}

void hashtable_delete(HashTable h, int x) {
  List p = hashtable_search(h, x);
  int index = hash_func(x, h->dim);

  if (p == NULL) {
    return;
  }

  if (p->prev == NULL) {
    h->array[index] = h->array[index]->next;
  } else {
    p->prev->next = p->next;

    if (p->next != NULL) {
      p->next->prev = p->prev;
    }
  }

  free(p);
}

int main() {
  HashTable h = hashtable_new(5);

  for (int i = 3; i < 35; i += 2) {
    hashtable_insert(h, i);
  }

  hashtable_print(h);
  printf("\n");

  List x = hashtable_search(h, 2);
  printf("Ricerca di 2: %s\n", (x == NULL) ? "NIL" : "FOUND");
  x = hashtable_search(h, 13);
  printf("Ricerca di 13: %s\n\n", (x == NULL) ? "NIL" : "FOUND");

  printf("Cancellazione di 13\n\n");
  hashtable_delete(h, 13);
  hashtable_print(h);

  return 0;
}