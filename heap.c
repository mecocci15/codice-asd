#include "heap.h"
#include "array.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// min heap = (<,>)
// max heap = (>,<)
#define ORDER_HEAPIFY <
#define ORDER_GO_UP >

void swap(int *arr, int i, int j) {
  int t = arr[i];
  arr[i] = arr[j];
  arr[j] = t;
}

int parent(int i) { return (i - 1) / 2; }

int left(int i) { return 2 * i + 1; }

int right(int i) { return 2 * i + 2; }

// crea un heap vuoto (se arr2 == NULL) o
// uno con elementi presi da arr2
Heap heap_create(int *arr2, int capacity) {
  Heap h = malloc(sizeof(Heap));

  h->heap_size = 0;
  h->length = capacity;
  h->arr = malloc(sizeof(int) * capacity);

  if (arr2 != NULL) {
    h->heap_size = capacity;

    for (int i = 0; i < capacity; i++) {
      h->arr[i] = arr2[i];
    }
  }

  return h;
}

// porta l'elem in pos i alla giusta altezza
// dal basso verso l'alto
void go_up(Heap h, int i) {
  while (i > 0 && h->arr[parent(i)] ORDER_GO_UP h->arr[i]) {
    swap(h->arr, i, parent(i));
    i = parent(i);
  }
}

// incrementa il valore di A[i] dentro l'heap
// impostandolo a key
void heap_increase_key(Heap h, int i, int key) {
  if (key < h->arr[i]) {
    exit(1);
  }

  h->arr[i] = key;
  go_up(h, i);
}

// inserisce key dentro l'heap e ripristina le proprietà
void heap_insert(Heap h, int key) {
  int i = h->heap_size;

  if (i >= h->length) {
    h->arr = realloc(h->arr, sizeof(int) * h->length * 2);
    h->length = h->length * 2;
  }

  h->heap_size += 1;
  h->arr[i] = key;

  go_up(h, i);
}

// stampa l'heap interno
void heap_print(Heap h) { print_array(h->arr, h->heap_size); }

// complessità: O(log n)
void heapify(Heap h, int i) {
  int l = left(i);
  int r = right(i);
  int max = i;

  if (l < h->heap_size && h->arr[l] ORDER_HEAPIFY h->arr[max]) {
    max = l;
  }

  if (r < h->heap_size && h->arr[r] ORDER_HEAPIFY h->arr[max]) {
    max = r;
  }

  if (max != i) {
    swap(h->arr, i, max);
    heapify(h, max);
  }
}

// trasforma l'array interno in un heap effettivo
// complessità: O(n)
void heap_build(Heap h) {
  h->heap_size = h->length;

  for (int i = h->length / 2; i >= 0; i--) {
    heapify(h, i);
  }
}

// complessità: O(nlog n)
void heap_sort(Heap h) {
  heap_build(h);

  for (int i = h->length - 1; i > 0; i--) {
    swap(h->arr, 0, i);
    h->heap_size -= 1;
    heapify(h, 0);
  }
}

// legge il massimo dall'heap
int heap_get_max(Heap h) {
  if (h->heap_size < 1) {
    printf("Error: underflow\n");
    exit(1);
  }

  return h->arr[0];
}

// estrae il massimo dall'heap e lo restituisce
int heap_extract_max(Heap h) {
  if (h->heap_size < 1) {
    printf("Error: underflow\n");
    exit(1);
  }

  int max = h->arr[0];
  h->arr[0] = h->arr[h->heap_size - 1];
  h->heap_size -= 1;
  heapify(h, 0);

  return max;
}

int main(int argc, char const *argv[]) {
  int arr2[] = {4, 2, 8, 9, 1};
  Heap h = heap_create(NULL, 5);

  heap_insert(h, 50);
  heap_insert(h, 12);
  heap_insert(h, 33);
  heap_insert(h, 21);
  heap_insert(h, 50);
  heap_print(h);

  heap_extract_max(h);
  heap_print(h);

  heap_insert(h, 40);
  heap_print(h);

  return 0;
}
