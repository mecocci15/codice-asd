#include "sorting.h"
#include "array.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void swap(int *arr, int i, int j) {
  int t = arr[i];
  arr[i] = arr[j];
  arr[j] = t;
}

// in place: sì
// stabile: sì
// complessità: O(n^2)
void bubble_sort(int *A, int n) {
  for (int i = 0; i < n - 1; i++) {
    for (int j = i + 1; j < n; j++) {
      if (A[j] < A[i]) {
        swap(A, i, j);
      }
    }
  }
}

// in place: sì
// stabile: no
// complessità: O(n^2)
void selection_sort(int *A, int n) {
  for (int i = 0; i < n - 1; i++) {
    int min = i;

    for (int j = i + 1; j < n; j++) {
      if (A[j] < A[min]) {
        min = j;
      }
    }

    swap(A, i, min);
  }
}

// in place: sì
// stabile: sì
// complessità: O(n^2)
void insertion_sort(int *A, int n) {
  for (int i = 1; i < n; i++) {
    int val = A[i];
    int j = i - 1;

    while (j >= 0 && A[j] > val) {
      A[j + 1] = A[j];
      j = j - 1;
    }

    A[j + 1] = val;
  }
}

void merge(int *A, int l, int m, int r) {
  int n1 = m - l + 1;
  int n2 = r - m;
  int L[n1];
  int R[n2];
  int i, j = 0;
  int k = l;

  // copio gli elementi di A in L ed R
  for (i = 0; i < n1; i++) {
    L[i] = A[l + i];
  }

  for (j = 0; j < n2; j++) {
    R[j] = A[m + 1 + j];
  }

  i = j = 0;

  // copio i primi min{n1, n2} elementi in A
  // seguendo l'ordine
  while (i < n1 && j < n2) {
    if (L[i] <= R[j]) {
      A[k] = L[i];
      i++;
    } else {
      A[k] = R[j];
      j++;
    }
    k++;
  }

  // copio gli elementi rimanenti di L ed R in A
  while (i < n1) {
    A[k] = L[i];
    i++;
    k++;
  }

  while (j < n2) {
    A[k] = R[j];
    j++;
    k++;
  }
}

void merge_sort_aux(int *A, int i, int j) {
  if (i < j) {
    int m = (i + j) / 2;

    merge_sort_aux(A, i, m);
    merge_sort_aux(A, m + 1, j);

    merge(A, i, m, j);
  }
}

void merge_sort(int *A, int n) { merge_sort_aux(A, 0, n - 1); }

int partition(int *A, int p, int q) {
  int pivot_index = p;

  if (pivot_index != p) {
    swap(A, p, pivot_index);
  }

  int pivot = A[p];
  int i = p + 1;
  int j = q;

  while (i <= j) {
    if (A[i] < pivot) {
      i++;
    } else if (A[j] > pivot) {
      j--;
    } else {
      swap(A, i, j);
      i++;
      j--;
    }
  }

  swap(A, p, j);

  return j;
}

void quick_sort_aux(int *A, int i, int j) {
  if (i < j) {
    int m = partition(A, i, j);

    quick_sort_aux(A, i, m);
    quick_sort_aux(A, m + 1, j);
  }
}

void quick_sort(int *A, int n) { quick_sort_aux(A, 0, n - 1); }
