#include "list.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

List cons(int x, List xs) {
  Node *new_node = malloc(sizeof(Node));

  new_node->key = x;
  new_node->next = xs;

  return new_node;
}

void list_print(List l) {
  while (l != NULL) {
    printf("%d ", l->key);
    l = l->next;
  }

  printf("\n");
}

// post: attacca la lista m alla l
List list_append(List l, List m) {
  if (l == NULL) {
    return m;
  } else {
    l->next = list_append(l->next, m);
    return l;
  }
}

int list_length(List l) {
  int length = 0;

  while (l != NULL) {
    length += 1;
    l = l->next;
  }

  return length;
}

// post: la lista l senza la prima occorrenza
//       di x se esiste
List list_delete(List l, int x) {
  Node *before_p = NULL;
  Node *p = l;

  while (p != NULL) {
    if (p->key == x) {
      break;
    }

    before_p = p;
    p = p->next;
  }

  if (before_p == NULL) {
    return l->next;
  } else {
    before_p->next = p->next;
    free(p);

    return l;
  }
}

// post: cancella tutte le occorrenze di x
List list_delete_all(List l, int x) {
  if (l == NULL) {
    return NULL;
  } else {
    if (x == l->key) {
      return list_delete_all(l->next, x);
    } else {
      l->next = list_delete_all(l->next, x);
      return l;
    }
  }
}

// post: true se esiste, false altrimenti
int list_search(List l, int x) {
  if (l == NULL) {
    return FALSE;
  } else {
    return l->key == x || list_search(l->next, x);
  }
}

// post: ritorna la lista inversa
List list_reverse_rec(List l) {
  if (l == NULL || l->next == NULL) {
    return l;
  } else {
    List r = list_reverse_rec(l->next);
    l->next->next = l;
    l->next = NULL;

    return r;
  }
}

List list_reverse_iter(List l) {
  List m = NULL;

  while (l != NULL) {
    List t = l->next;
    l->next = m;
    m = l;
    l = t;
  }

  return m;
}

List list_clone_iter(List l) {
  List r = cons(-1, NULL);
  List c = r;

  while (l != NULL) {
    c->next = cons(l->key, NULL);
    c = c->next;
    l = l->next;
  }

  return r->next;
}

List list_clone_rec(List l) {
  if (l == NULL) {
    return NULL;
  } else {
    return cons(l->key, list_clone_rec(l->next));
  }
}

// post: x inserito davanti all'elemento in posto i
List list_insert_at_rec(int x, int i, List l) {
  if (i == 0) {
    return cons(x, l);
  } else {
    l->next = list_insert_at_rec(x, i - 1, l->next);

    return l;
  }
}

// post: x inserito davanti all'elemento in posto i
List list_insert_at_iter(int x, int i, List l) {
  List bp = NULL;
  List p = l;

  while (i > 0 && p != NULL) {
    bp = l;
    p = p->next;
    i--;
  }

  if (bp == NULL) {
    return cons(x, l);
  } else {
    bp->next = cons(x, p);

    return l;
  }
}

List array_to_list(int arr[], int n) {
  List l = NULL;

  for (int i = n - 1; i >= 0; i--) {
    l = cons(arr[i], l);
  }

  return l;
}
