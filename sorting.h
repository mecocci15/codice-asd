#ifndef SORT_H
#define SORT_H

void bubble_sort(int *arr, int n);

void selection_sort(int *A, int n);

void insertion_sort(int *A, int n);

void merge_sort(int *A, int n);

void quick_sort(int *A, int n);

#endif