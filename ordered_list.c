#include "list.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// post: restituisce la lista l con x
List list_insert_ordered(List l, int x) {
  if (l == NULL || x < l->key) {
    return cons(x, l);
  } else {
    if (x == l->key) {
      return l;
    } else {
      l->next = list_insert_ordered(l->next, x);
      return l;
    }
  }
}

List list_delete_ordered_all(List l, int x) {
  if (l == NULL) {
    return NULL;
  } else {
    if (l->key == x) {
      return list_delete_ordered_all(l->next, x);
    } else if (x < l->key) {
      return l;
    } else {
      l->next = list_delete_ordered_all(l->next, x);
      return l;
    }
  }
}

List list_delete_ordered(List l, int x) {
  if (l == NULL || x < l->key) {
    return NULL;
  } else {
    if (l->key == x) {
      return l->next;
    } else {
      l->next = list_delete_ordered(l->next, x);
      return l;
    }
  }
}

// pre: l ordinata
// post: true se x esiste
int list_search_ordered(List l, int x) {
  if (l == NULL || x < l->key) {
    return FALSE;
  } else {
    if (x == l->key) {
      return TRUE;
    } else {
      return list_search_ordered(l->next, x);
    }
  }
}

// post: una nuova lista costruita dalle intersezioni
//
// es. [2,3,5,8], [3,4,8] -> [3,8]
List list_intersect(List l, List m) {
  if (l == NULL || m == NULL) {
    return NULL;
  } else {
    if (l->key == m->key) {
      return cons(l->key, list_intersect(l->next, m->next));
    } else if (l->key < m->key) {
      return list_intersect(l->next, m);
    } else {
      return list_intersect(l, m->next);
    }
  }
}

// post: una nuova lista costruita dalle unioni
//
// es. [2,3,5,8], [3,4,8] -> [2,3,4,5,8]
List list_union(List l, List m) {
  if (l == NULL) {
    return list_clone_iter(m);
  } else if (m == NULL) {
    return list_clone_iter(l);
  } else {
    if (l->key == m->key) {
      return cons(l->key, list_union(l->next, m->next));
    } else if (l->key < m->key) {
      return cons(l->key, list_union(l->next, m));
    } else {
      return cons(m->key, list_union(l, m->next));
    }
  }
}

// post: una nuova lista costruita dalle differenze
//
// es. [2,3,5,8], [3,4,8] -> [2,5]
List list_diff(List l, List m) {
  if (l == NULL || m == NULL) {
    return l;
  } else {
    if (l->key == m->key) {
      return list_diff(l->next, m->next);
    } else if (l->key < m->key) {
      return cons(l->key, list_diff(l->next, m));
    } else {
      return list_diff(l, m->next);
    }
  }
}

List _merge(List l, List m) {
  if (l == NULL) {
    return m;
  } else if (m == NULL) {
    return l;
  }

  if (l->key <= m->key) {
    return cons(l->key, _merge(l->next, m));
  } else { // l->key > m->key
    return cons(m->key, _merge(l, m->next));
  }
}

List _split_v1(List l) {
  int n = list_length(l) / 2;
  List m = NULL;

  while (n > 0) {
    m = l;
    l = l->next;
    n--;
  }

  m->next = NULL;

  return l;
}

List _split_v2(List l) {
  List p = l;
  List q = l;

  while (q->next != NULL && q->next->next != NULL) {
    q = q->next->next;
    p = p->next;
  }

  q = p->next;
  p->next = NULL;

  return q;
}

List mergesort(List l) {
  if (l == NULL || l->next == NULL) {
    return l;
  } else {
    List m = _split_v2(l);

    l = mergesort(l);
    m = mergesort(m);

    return _merge(l, m);
  }
}

int main(int argc, char const *argv[]) {
  int arr[] = {5, 8, 2, 3, 3};
  List m = NULL;
  int arr2[] = {3, 8, 4};
  List n = NULL;

  for (int i = 0; i < 5; i++) {
    m = list_insert_ordered(m, arr[i]);
  }

  for (int i = 0; i < 3; i++) {
    n = list_insert_ordered(n, arr2[i]);
  }

  list_print(m);
  list_print(n);

  printf("\nIntersezione: ");
  list_print(list_intersect(m, n));
  printf("Unione: ");
  list_print(list_union(m, n));
  printf("Differenza: ");
  list_print(list_diff(m, n));

  printf("\nMergesort su liste:\nPrima: ");
  int arr3[] = {2, 8, 9, 5, 3};
  List o = array_to_list(arr3, 5);
  list_print(o);

  printf("Dopo: ");
  List x = mergesort(o);
  list_print(x);

  return 0;
}
