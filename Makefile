run-heap:
	@gcc -c array.c
	@gcc -o heap array.o heap.c
	@./heap

run-sorts:
	@gcc -c array.c
	@gcc -c sorting.c
	@gcc -o sort array.o sorting.o sorting_main.c
	@./sort

run-search:
	@gcc -c array.c
	@gcc -o search array.o search.c
	@./search

compile:
	@gcc -o btree btree.c
	@gcc -o ktree ktree.c
	@gcc -o bst bst.c
	@gcc -c list.c
	@gcc -o list list.o list_main.c
	@gcc -o ordered_list list.o ordered_list.c
	@gcc -o hash_open hash_open.c
	@gcc -o hash_concat hash_concat.c
	@gcc -o queue queue.c
	@gcc -o stack stack.c

clean:
	@rm -f bst btree ktree
	@rm -f hash_concat hash_open
	@rm -f queue stack
	@rm -f list.o ordered_list
	@rm -f search
	@rm -f array.o sorting.o sort
	@rm -f heap
