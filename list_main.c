#include "list.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// post: ritorna la lista delle somme a partire da... (distruttivamente)
//
// es. [1, 2, 3] -> [6, 5, 3]
int list_rank(List l) {
  if (l == NULL) {
    return 0;
  } else {
    l->key = l->key + list_rank(l->next);

    return l->key;
  }
}

int main(int argc, char const *argv[]) {
  List l = cons(1, cons(2, cons(3, NULL)));

  list_print(l);

  printf("Lunghezza: %d\n", list_length(l));

  printf("Lista inversa: ");
  list_print(list_reverse_iter(l));

  return 0;
}

