#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node {
  int key;
  struct Node *left;
  struct Node *right;
} Node;

typedef struct Node *BTree;

BTree btree_cons(int key, BTree l, BTree r) {
  BTree rootnode = malloc(sizeof(Node));

  rootnode->key = key;
  rootnode->left = l;
  rootnode->right = r;

  return rootnode;
}

void btree_print_aux(BTree bt, int n) {
  if (bt != NULL) {
    for (int i = 0; i < n; i++)
      printf("   ");

    printf("%d\n", bt->key);
    btree_print_aux(bt->left, n + 1);
    btree_print_aux(bt->right, n + 1);
  }
}

void btree_print(BTree bt) { btree_print_aux(bt, 0); }

int min(int a, int b) {
  if (a < b) {
    return a;
  } else {
    return b;
  }
}

int max(int a, int b) {
  if (a > b) {
    return a;
  } else {
    return b;
  }
}

// pre: t albero non vuoto
int btree_min(BTree t) {
  int r = t->key;

  if (t->left != NULL) {
    r = min(r, btree_min(t->left));
  }

  if (t->right != NULL) {
    r = min(r, btree_min(t->right));
  }

  return r;
}

BTree btree_search(BTree t, int x) {
  if (t == NULL) {
    return NULL;
  } else {
    if (t->key == x) {
      return t;
    } else {
      BTree left = btree_search(t->left, x);
      BTree right = btree_search(t->right, x);

      if (left != NULL) {
        return left;
      } else {
        return right;
      }
    }
  }
}

// pre: t non vuoto
// post: altezza albero (max dei livelli)
int btree_height(BTree t) {
  if (t->left == NULL && t->right == NULL) {
    return 0;
  } else {
    int hl = 0;
    int hr = 0;

    if (t->left != NULL) {
      hl = btree_height(t->left);
    }

    if (t->right != NULL) {
      hr = btree_height(t->right);
    }

    return 1 + max(hr, hl);
  }
}

int main() {
  BTree a = btree_cons(5, NULL, NULL);
  BTree b =
      btree_cons(35, btree_cons(9, NULL, NULL), btree_cons(1, NULL, NULL));
  BTree bt = btree_cons(20, b, a);

  btree_print(bt);

  printf("\nMinimo: %d\n", btree_min(bt));

  printf("\nAltezza: %d\n", btree_height(bt));

  return 0;
}
